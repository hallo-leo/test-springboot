package com.hallo.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
    /***
     * 测试hello
     * @return: string
     */
    @GetMapping("/hello")
    public String hello() {
        return "Hello !!!!";
    }
}

